﻿(function () {
    var main = angular.module("app");
    main.factory("backgroundServices", function (serverHostManager) {
        function register() {
            //cordova.plugins.backgroundMode.setDefaults({
            //    text: 'TrackMyWay is running in the background',
            //    ticker: 'TrackMyWay is running in the background',
            //    title: 'TrackMyWay',
            //});
            //cordova.plugins.backgroundMode.enable();
            //cordova.plugins.backgroundMode.onactivate = function () {
            //    cordova.plugins.backgroundMode.configure({
            //        ticker: "putko ma faniiii",
            //    });
            //};

            cordova.plugins.backgroundMode.onfailure = function (errorCode) {
                debugger;
            };

            //cordova.plugins.notification.local.registerPermission(function (granted) {
            //    debugger;
            //});

            //cordova.plugins.notification.local.hasPermission(function (granted) {
            //    debugger;
            //});

            cordova.plugins.notification.local.on("click", function (notification) {
                debugger;
            });

            serverHostManager.onRequestTracking(function (sender) {
                var user = JSON.parse(sender);
                var message = user.FirstName + " wants to track your location";

                cordova.plugins.notification.local.schedule({
                    title: 'TrackMyWay',
                    text: message,
                    data: { meetingId: "#123FG8" },
                });

                // Modify the currently displayed notification
                //cordova.plugins.backgroundMode.configure({
                //    ticker: message,
                //});
            });
        }

        return {
            register: register
        }
    });
})();