﻿function initMap(div) {
    var map = plugin.google.maps.Map.getMap(div);
    map.addEventListener(plugin.google.maps.event.MAP_READY, function () {
        map.showDialog();
    }, false);

    var socket = io('http://whereru-kokata.rhcloud.com:8000');
    var geolocationOptions = {
        enableHighAccuracy: true,
        maximumAge: 0
    };
    navigator.geolocation.watchPosition(onWatchPositionSuccess, errorLog, geolocationOptions);

    function errorLog(error) {
        navigator.notification.alert(error.message);
    };

    var isLoaded = false;
    var isAnimationInProgress = false;
    var markers = [];

    socket.on("send-android", function (data) {
        var position = JSON.parse(data);

        if (!isLoaded) {
            isAnimationInProgress = true;
        }

        var pos = new plugin.google.maps.LatLng(position.Geopoint.Position.Latitude, position.Geopoint.Position.Longitude);
        var cameraSettings = {
            'target': pos,
            'tilt': 0,
            'zoom': 18,
            'bearing': 140
        };
        if (!isAnimationInProgress && isLoaded) {
            map.moveCamera(cameraSettings);
        }
        else {
            map.animateCamera(cameraSettings, function () {
                isLoaded = true;
                isAnimationInProgress = false;
            });
        }
        if (markers)
            markers.forEach(function (m) { m.remove(); })

        map.addMarker({ 'position': pos }, function (marker) {
            markers.push(marker);
        });
    });

    function onWatchPositionSuccess(position) {
        var payload = JSON.stringify({
            Geopoint: {
                Position: {
                    Latitude: position.coords.latitude,
                    Longitude: position.coords.longitude
                }
            }
        });
        socket.emit("android", payload);
    };
};
