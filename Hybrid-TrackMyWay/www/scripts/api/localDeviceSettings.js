﻿(function () {
    var app = angular.module("app");
    app.factory('localDeviceSettings', function () {
        var currentVersion = 3.04;

        var userKey = "current-user-key";
        var versionKey = "current-version-key";

        function setCurrentUserSettings(data) {
            localStorage.setItem(userKey, data === null ? null : JSON.stringify(data));
        }

        function getUser() {
            var user = localStorage.getItem(userKey);
            return JSON.parse(user);
        }

        function isNewVersionAvailable() {
            var version = localStorage.getItem(versionKey) || 0;
            return +version < currentVersion;
        }

        function updateCurrentVersion() {
            localStorage.setItem(versionKey, currentVersion.toString());
        }

        return {
            getUser: getUser,
            setCurrentUserSettings: setCurrentUserSettings,
            isNewVersionAvailable: isNewVersionAvailable,
            updateCurrentVersion: updateCurrentVersion
        }
    });
})();

