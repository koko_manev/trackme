﻿(function () {
    var main = angular.module("app");
    main.factory("serverHostManager", function (localDeviceSettings) {
        var live_url = 'http://whereru-kokata.rhcloud.com:8000';
        //live_url = "ws://192.168.1.147:3001";
        var socket = io(live_url);

        function on(event, callback) {
            socket.on(event, callback);
        }

        function emit(event, payload) {
            if (payload)
                socket.emit(event, typeof payload === "object" ? JSON.stringify(payload) : payload);
            else
                socket.emit(event);
        }

        return {
            emitLoginUser: function () {
                var user = localDeviceSettings.getUser();
                emit("loggin-user-event", user);
            },
            getAllRegisteredUsers: function (callback) {
                on("get-all-registered-users-event", function (data) {
                    var res = JSON.parse(data);
                    callback(res);
                });
            },
            registerUser: function (user) {
                var user = localDeviceSettings.getUser();
                emit("add-user-event", user);
            },
            onRequestTracking: function (callback) {
                var currentUser = localDeviceSettings.getUser();
                var requestUserEvent = currentUser.Phone.Number + "-request-user-event";
                on(requestUserEvent, callback);
            },
            onRequestTrackingResult: function (callback) {
                on("request-user-event-result", function (result) {
                    if (result == null) return;
                    var res = JSON.parse(result);
                    callback(res);
                });
            },
            offRequestTrackingResult: function () {
                socket.off("request-user-event-result");
            },
            requestTrackingResponse: function (sender, isAccepted) {
                emit("request-user-track-result",
                    JSON.stringify({
                        SenderUser: sender,
                        IsAccepted: isAccepted
                    }));
            },
            requestTracking: function (user) {
                emit("request-user-track", JSON.stringify(user));
            },
            onPositionRecieved: function (callback) {
                on("send-position-event", function (param) {
                    var args = JSON.parse(param);
                    callback(args);
                });
            },
            sendPosition: function (receivers, position) {
                receivers.forEach(function (r) {
                    socket.emit("send-position-event", JSON.stringify(r), JSON.stringify(position));
                });
            },
            onStopUserTrackRecieved: function (callback) {
                on("stop-user-tracking", function (user) {
                    callback(JSON.parse(user));
                });
            },
            emitDisconnect: function () {
                var user = localDeviceSettings.getUser();
                emit("stop-user-tracking", user);
            },
            onUserDisconnect: function (callback) {
                on("disonnect-user", function (user) {
                    callback(JSON.parse(user));
                });
            }
        }
    });
})();