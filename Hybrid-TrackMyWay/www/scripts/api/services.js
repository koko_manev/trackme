﻿(function () {
    var main = angular.module("app");
    main.factory("receivers", function () {
        var receivers = [];

        return {
            set: function (sender) {
                receivers.push(sender);
            },
            get: function () {
                return receivers;
            }
        }
    });
})();