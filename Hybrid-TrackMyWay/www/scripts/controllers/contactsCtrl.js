﻿(function () {
    "use strict";
    var main = angular.module("app");
    main.controller("contactsCtrl",
        function ($scope,
                  $location,
                  $rootScope,
                  $ionicPopup,
                  localDeviceSettings,
                  serverHostManager,
                  countriesInfo,
                  receivers) {

            var viewModel = {
                contacts: [],
                hasContacts: null,
                startTracking: function (contact) {
                    if (!contact.IsOnline) {
                        $ionicPopup.alert({
                            title: 'Warning!',
                            template: 'User is offline',
                        });
                        return;
                    }
                    //$ionicLoading.show({template: 'Loading...'//});
                    var alertPopup = $ionicPopup.alert({
                        title: 'Waiting for response...',
                        template: '<div id="spinner"><ion-spinner icon="dots" class="spinner-dark"></ion-spinner></div>',
                        okText: 'Cancel',
                        cssClass: 'waitingResponse'
                    });
                    serverHostManager.onRequestTrackingResult(function (requestResult) {
                        if (requestResult.IsAccepted) {
                            receivers.set(requestResult.SenderUser);
                            $location.path("/map");
                            $rootScope.$apply();
                        } else {
                            $ionicPopup.alert({ title: 'Request cancelled' });
                        }
                        alertPopup.close();
                        serverHostManager.offRequestTrackingResult();
                    });
                    serverHostManager.requestTracking(contact);
                }
            };

            function onSuccess(contacts) {
                serverHostManager.getAllRegisteredUsers(function (users) {
                    viewModel.contacts = [];
                    var currentUser = localDeviceSettings.getUser();
                    var validUsers = users.filter(function (user) {
                        return currentUser.Phone.Number !== user.Phone.Number;
                    });
                    var country = countriesInfo.filter(function (item) {
                        return item.alpha2 === currentUser.CountryCode
                    })[0];
                    contacts.forEach(function (contact) {
                        if (contact.phoneNumbers) {
                            contact.phoneNumbers.forEach(function (phone) {
                                validUsers.forEach(function (user) {
                                    var number = user.Phone.Number;
                                    if (!phone.value.startsWith(country.countryCallingCodes[0]))
                                        number = user.Phone.Number.replace(country.countryCallingCodes[0], 0);

                                    if (phone.value.replace(/\s/g, '') === number)
                                        if (viewModel.contacts.indexOf(user) === -1)
                                            viewModel.contacts.push(user);

                                });
                            });
                        }
                    });
                    viewModel.hasContacts = viewModel.contacts.length > 0;
                    $scope.$digest();
                });
                serverHostManager.emitLoginUser();
                serverHostManager.onRequestTracking(function (sender) {
                    var user = JSON.parse(sender);
                    var message = user.FirstName + " wants to track your location";
                    var confirmPopup = $ionicPopup.confirm({
                        title: 'Tracking request recieved',
                        template: message
                    });
                    confirmPopup.then(function (res) {
                        var isAccepted = res === true;
                        serverHostManager.requestTrackingResponse(user, isAccepted);
                        if (isAccepted) {
                            receivers.set(user);
                            $location.path("/map");
                            $rootScope.$apply();
                        }
                    });
                });
            };

            function onError(contactError) {
                console.log(contactError);
            };

            serverHostManager.onUserDisconnect(function (user) {
                if (viewModel.hasContacts) {
                    viewModel.contacts.forEach(function (contact) {
                        if (contact.Phone.Number === user.Phone.Number) {
                            contact.IsOnline = false;
                            $scope.$digest();
                            return;
                        }
                    });
                }
            });

            var options = new ContactFindOptions();
            var fields = ["*"];
            navigator.contacts.find(fields, onSuccess, onError);

            $scope.viewModel = viewModel;
        });
})();