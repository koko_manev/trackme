﻿(function () {
    "use strict";

    var main = angular.module("app");

    main.controller("mapCtrl", function ($scope, $ionicPlatform, $ionicPopup, serverHostManager) {
        var backHandler = $ionicPlatform.registerBackButtonAction(function () {
            var message = 'Are you sure you want to disconnect?';
            navigator.notification.confirm('', function (result) {
                var isAccepted = result === 1;
                if (isAccepted) {
                    //serverHostManager.emitDisconnect();
                    navigator.app.backHistory();
                }
            }, message);
        }, 100);
        $scope.$on("$destroy", backHandler);
    });
})();