﻿(function () {
    "use strict";

    var main = angular.module("app");

    main.controller("registerCtrl", function ($scope, $location, localDeviceSettings, serverHostManager, countriesInfo) {
        navigator.globalization.getLocaleName(function (result) {
            var code = result.value.split('-');
            code = code.length === 1 ? code[0] : code[1];

            $scope.viewModel = {
                user: {
                    FirstName: null,
                    Phone: {
                        Number: "",
                        Kind: 1 //// Mobile
                    },
                    CountryCode: code
                },
                countriesInfo: countriesInfo,
                register: function () {
                    if ($scope.regForm.$valid) {
                        var code = this.user.CountryCode;

                        var country = countriesInfo.filter(function (item) {
                            return item.alpha2 === code;
                        })[0];

                        this.user.Phone.Number = country.countryCallingCodes[0] + this.user.Phone.Number;
                        localDeviceSettings.setCurrentUserSettings(this.user);
                        serverHostManager.registerUser();
                        $location.path("/contacts");
                    }
                }
            };
        });

    });
})();