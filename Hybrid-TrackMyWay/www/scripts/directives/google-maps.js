﻿(function () {
    "use strict";

    var main = angular.module("app");

    main.directive("googleMaps", function (serverHostManager, receivers, $rootScope, $location, $ionicPopup) {
        return {
            restrict: "E",
            templateUrl: "scripts/directives/google-maps.html",
            link: function (scope, el, attr) {
                var map;
                function initMap(div) {
                    var isLoaded = false;
                    var isAnimationInProgress = false;
                    var _receivers = receivers.get();
                    var zoom = 17;
                    var isMapDragged = false;

                    map = plugin.google.maps.Map.getMap(div);
                    map.addEventListener(plugin.google.maps.event.CAMERA_CHANGE, function (args) {
                        if (isLoaded) {
                            zoom = args.zoom;
                            //isMapDragged = true;
                        }
                    });

                    map.on(plugin.google.maps.event.MAP_READY, function () {
                        //map.showDialog();
                        map.setDiv($("#map_canvas")[0]);
                    });

                    var geolocationOptions = {
                        enableHighAccuracy: true,
                        maximumAge: 0
                    };
                    navigator.geolocation.watchPosition(onWatchPositionSuccess, errorLog, geolocationOptions);

                    function errorLog(error) {
                        navigator.notification.alert(error.message);
                    };

                    serverHostManager.onPositionRecieved(function (position) {
                        if (!isLoaded) {
                            isAnimationInProgress = true;
                        }
                        var _position = position.Geopoint.Position;
                        var pos = new plugin.google.maps.LatLng(_position.Latitude, _position.Longitude);
                        var cameraSettings = {
                            'target': pos,
                            'tilt': 0,
                            'zoom': zoom,
                            'bearing': 140
                        };

                        if (!isAnimationInProgress && isLoaded && !isMapDragged) {
                            map.moveCamera(cameraSettings);
                        }
                        else if (!isMapDragged) {
                            map.animateCamera(cameraSettings, function () {
                                isLoaded = true;
                                isAnimationInProgress = false;
                            });
                        }

                        map.clear();
                        map.addMarker({
                            position: pos,
                            title: _receivers[0].FirstName
                        }, function (marker) {
                            marker.showInfoWindow();
                        });
                    });

                    function onWatchPositionSuccess(position) {
                        var payload = {
                            Geopoint: {
                                Position: {
                                    Latitude: position.coords.latitude,
                                    Longitude: position.coords.longitude
                                }
                            }
                        };
                        serverHostManager.sendPosition(_receivers, payload);
                    };
                };

                plugin.google.maps.Map.isAvailable(function (isAvailable, message) {
                    if (isAvailable)
                        initMap(angular.element(el, "map_canvas"));
                    else
                        $ionicPopup.alert({ title: message });
                });

                serverHostManager.onStopUserTrackRecieved(function (user) {
                    var message = user.FirstName + " has been disconnected.";
                    navigator.notification.alert(message, function () {
                        if (map)
                            map.closeDialog();

                        $location.path("/contacts");
                        $rootScope.$apply();
                    }, "", "Back to contacts");
                });
            }
        };
    });
})();