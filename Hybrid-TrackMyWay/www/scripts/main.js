﻿(function () {
    "use strict";

    var main = angular.module("app", ["ngRoute", "ionic"]);
    main.config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                template: "",
                controller: "mainCtrl"
            })
            .when("/register", {
                templateUrl: "views/register-view.html",
                controller: "registerCtrl"
            })
            .when("/contacts", {
                templateUrl: "views/contacts-view.html",
                controller: "contactsCtrl"
            })
            .when("/map", {
                templateUrl: "views/maps-view.html",
                controller: "mapCtrl"
            });
    });

    main.controller("mainCtrl", function ($scope, $location, $ionicPlatform, localDeviceSettings, backgroundServices) {
        if (localDeviceSettings.isNewVersionAvailable()) {
            localDeviceSettings.setCurrentUserSettings(null);
        }
        var user = localDeviceSettings.getUser();
        if (!user) {
            $location.path("/register");
            localDeviceSettings.updateCurrentVersion();
        } else {
            $location.path("/contacts");
        }

        document.addEventListener('deviceready', onDeviceReady, false);

        function onDeviceReady() {
            document.addEventListener('pause', onPause.bind(this), false);
            document.addEventListener('resume', onResume.bind(this), false);

            //backgroundServices.register();
        };

        function onPause() {
        };

        function onResume() {
        };
    });
})();