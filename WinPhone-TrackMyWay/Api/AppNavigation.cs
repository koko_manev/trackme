﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace TrackMyWay.Api
{
    internal class AppNavigation
    {
        public static bool Navigate<T>(object parameter = null)
        {
            bool navigate = ((Frame)Window.Current.Content).Navigate(typeof(T), parameter);

            if (!navigate)
                throw new Exception("Failed to navigate to" + typeof(T).FullName);

            return navigate;
        }
    }
}
