﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Networking.Sockets;

namespace TrackMyWay.Api.BackgroundTasks
{
    class GpsSenderTask : IBackgroundTask
    {
        private bool taskRegistered = false;
        public static Guid Id;
        public static Guid GpsSenderTaskKey = new Guid("C60063CE-0589-40E6-9BBE-03A27CC4A217");

        public GpsSenderTask()
        {
        }

        public void Run(IBackgroundTaskInstance taskInstance)
        {
        }

        internal static void Register()
        {
            

            if (IsTaskRegistered())
                return;

            var builder = new BackgroundTaskBuilder();
            builder.Name = "TrackMyWay.GpsSenderTask";
            builder.TaskEntryPoint = "TrackMyWay.Api.BackgroundTasks.GpsSenderTask";
            builder.SetTrigger(new SystemTrigger(SystemTriggerType.UserAway, false));
            builder.SetTrigger(new PushNotificationTrigger());

            var taskRegistration = builder.Register();
            Id = taskRegistration.TaskId;

            taskRegistration.Completed += TaskRegistrationCompleted;
        }

        private static void TaskRegistrationCompleted(BackgroundTaskRegistration sender, BackgroundTaskCompletedEventArgs args)
        {
            //BackgroundExecutionManager.RequestAccessAsync()
        }

        private static bool IsTaskRegistered()
        {
            return BackgroundTaskRegistration.AllTasks.Any(t => t.Key == Id || t.Value.Name == "TrackMyWay.GpsSenderTask");
        }
    }
}
