﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrackMyWay.Api.Models;
using Windows.ApplicationModel.Contacts;
using Windows.Storage;
using System.Linq;
using TrackMe.Api;

namespace TrackMyWay.Api
{
    internal static class ClientDeviceSettings
    {
        private static Version AppVersion = Version.Parse("1.0.0.0");

        private const string CURRENT_PHONE_CLIENT_DATA = "CURRENT-PHONE-CLIENT-DATA";

        private const string AVAILABLE_USERS_CACHE = "AVAILABLE-USERS-CACHE";

        private const string SETTINGS_VERSION = "SETTINGS-VERSION";

        internal static bool IsNewVersionAvailable()
        {
            var settings = ApplicationData.Current.LocalSettings;
            var ver = settings.Values[SETTINGS_VERSION] as string;
            if (ver == null)
                return new Version(0, 0) < AppVersion;
            return JsonConvert.DeserializeObject<Version>(ver) < AppVersion;
        }

        internal static void UpdateCurrentVersion()
        {
            var settings = ApplicationData.Current.LocalSettings;

            settings.Values[SETTINGS_VERSION] = JsonConvert.SerializeObject(AppVersion);
        }

        internal static bool IsCurrentUserSet()
        {
            var settings = ApplicationData.Current.LocalSettings;

            return settings.Values[CURRENT_PHONE_CLIENT_DATA] != null;
        }

        internal static User GetCurrentUser()
        {
            var settings = ApplicationData.Current.LocalSettings;

            if (!IsCurrentUserSet())
            {
                return null;
            }
            var userLocalSettings = settings.Values[CURRENT_PHONE_CLIENT_DATA] as string;

            return JsonConvert.DeserializeObject<User>(userLocalSettings);
        }

        internal static void SetCurrentUser(User user)
        {
            var settings = ApplicationData.Current.LocalSettings;

            //// Set phone number validation!
            settings.Values[CURRENT_PHONE_CLIENT_DATA] = user != null ? JsonConvert.SerializeObject(user) : null;
        }

        internal static void StoreAvailableUserInCache(IList<User> availableUsers)
        {
            if (availableUsers == null || availableUsers.Count == 0)
                return;

            var settings = ApplicationData.Current.LocalSettings;

            settings.Values[AVAILABLE_USERS_CACHE] = JsonConvert.SerializeObject(availableUsers);
        }

        private static IEnumerable<User> GetUsersFromCache()
        {
            var settings = ApplicationData.Current.LocalSettings;

            var users = settings.Values[AVAILABLE_USERS_CACHE] as string;
            if (users == null)
                return Enumerable.Empty<User>();

            return JsonConvert.DeserializeObject<IList<User>>(users);
        }

        internal static async Task<IList<User>> GetContacts(IEnumerable<User> users)
        {
            var cachedUsers = GetUsersFromCache();
            if (cachedUsers != null && cachedUsers.Any())
                return cachedUsers.ToList();

            var currentUser = GetCurrentUser();
            var countriInfo = CountryInfoList.Get.Find(c => c.Alpha2 == currentUser.CountryCode);
            var contacts = await GetUserContactsAsync();
            var searchUsers = users.Where(u => u.Phone.Number != currentUser.Phone.Number).ToList();

            return searchUsers.Count == 0 ? null : contacts.SelectMany(c => SelectContacts(c, countriInfo.CountryCallingCodes[0], searchUsers)).Distinct().ToList();
        }

        private static IEnumerable<User> SelectContacts(Contact contact, string countryCode, IList<User> users)
        {
            if (!contact.Phones.Any() || users.Count == 0)
                yield break;

            foreach (User user in users)
                foreach (var phone in contact.Phones)
                {
                    string number = phone.Number;

                    if (!number.StartsWith(countryCode))
                        number = user.Phone.Number.Replace(countryCode, "0");

                    if (phone.Number == number)
                        yield return user;
                }
        }

        internal async static Task<IReadOnlyList<Contact>> GetUserContactsAsync()
        {
            //ContactPicker
            ContactStore store = await ContactManager.RequestStoreAsync();
            var contacts = await store.FindContactsAsync();
            return contacts;
        }
    }
}
