﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using TrackMyWay.Api.Models;

namespace TrackMe.Api
{
    internal class CountryInfoList
    {
        private static string source = "TrackMyWay.CountriesInfo.json";

        public static List<CountryInfo> Get
        {
            get
            {
                var stream = typeof(CountryInfoList).GetTypeInfo().Assembly.GetManifestResourceStream(source);
                using (var reader = new StreamReader(stream))
                {
                    string data = reader.ReadToEnd();

                    return JsonConvert.DeserializeObject<List<CountryInfo>>(data);
                }
            }
        }
    }
}
