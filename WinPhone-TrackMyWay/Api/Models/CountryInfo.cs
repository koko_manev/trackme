﻿namespace TrackMyWay.Api.Models
{
    internal class CountryInfo
    {
        public string Alpha2 { get; set; }
        public string Alpha3 { get; set; }
        public string[] CountryCallingCodes { get; set; }
        public string[] Currencies { get; set; }
        public string Name { get; set; }
    }
}
