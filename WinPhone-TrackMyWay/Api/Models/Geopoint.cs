﻿namespace TrackMyWay.Api.Models
{
    internal class Geopoint
    {
        public Position Position { get; set; }
    }
}
