﻿namespace TrackMyWay.Api.Models
{
    internal struct Position
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double Altitude { get; set; }
    }
}
