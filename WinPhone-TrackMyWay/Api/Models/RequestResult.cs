﻿namespace TrackMyWay.Api.Models
{
    internal class RequestResult
    {
        public User SenderUser { get; set; }

        public bool IsAccepted { get; set; }
    }
}
