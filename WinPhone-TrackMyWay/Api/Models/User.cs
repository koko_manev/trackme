﻿using Windows.ApplicationModel.Contacts;

namespace TrackMyWay.Api.Models
{
    internal class User
    {
        public User() { }

        public string CountryCode;

        public string FirstName { get; set; }

        public ContactPhone Phone { get; set; }
    }
}
