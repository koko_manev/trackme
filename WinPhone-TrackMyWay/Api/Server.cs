﻿using Newtonsoft.Json;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using TrackMyWay.Api.Models;

namespace TrackMyWay.Api
{
    internal static class Server
    {
#if DEBUG
        private const string URL = "http://whereru-kokata.rhcloud.com:8000";
        //private const string URL = "ws://192.168.1.147:3001";
#else 
        //private const string URL = "ws://192.168.1.147:3001";
        private const string URL = "http://whereru-kokata.rhcloud.com:8000";
#endif

        private static Socket socket;

        static Server()
        {
            socket = IO.Socket(URL);
        }

        public static void RegisterUserInfo(User user)
        {
            socket.Emit("add-user-event", JsonConvert.SerializeObject(user));
        }

        public static void AttachedErrorRecieved()
        {
            socket.On("log", res =>
            {
                Debug.WriteLine(res);
            });

            socket.Emit("add-user-event");
        }

        public static void EmitLoginUser()
        {
            User user = ClientDeviceSettings.GetCurrentUser();
            socket.Emit("loggin-user-event", JsonConvert.SerializeObject(user));
        }

        public static void RegisteredForUsers(Action<IEnumerable<User>> callback)
        {
            socket.On("get-all-registered-users-event", payload =>
            {
                if (payload == null) return;

                var allRegisteredUsers = JsonConvert.DeserializeObject<IEnumerable<User>>(payload.ToString());

                callback(allRegisteredUsers);
            });
        }

        public static void RequestTracking(User user)
        {
            var requestTrackResultEvent = "request-user-track";
            socket.Emit(requestTrackResultEvent, JsonConvert.SerializeObject(user));
        }

        internal static void EmitDisconnect(User trackingUser)
        {
            socket.Emit("stop-user-tracking", JsonConvert.SerializeObject(trackingUser));
        }

        public static void RequestTrackingResponse(User sender, bool isAllowed)
        {
            socket.Emit("request-user-track-result",
                JsonConvert.SerializeObject(new RequestResult { IsAccepted = isAllowed, SenderUser = sender }));
        }

        public static void OnRequestTracking(Action<User> callback)
        {
            User user = ClientDeviceSettings.GetCurrentUser();
            var requestUserEvent = user.Phone.Number + "-request-user-event";
            socket.On(requestUserEvent, result =>
            {
                if (result == null) return;
                var requestUser = JsonConvert.DeserializeObject<User>(result.ToString());
                callback(requestUser);
            });
        }

        public static void OnRequestTrackingResult(Action<RequestResult> callback)
        {
            socket.On("request-user-event-result", result =>
            {
                if (result == null) return;

                var res = JsonConvert.DeserializeObject<RequestResult>(result.ToString());
                callback(res);
            });
        }

        public static void OffRequestTrackingResult()
        {
            socket.Off("request-user-event-result");
        }

        public static void EmitPosition(User target, DTO args)
        {
            socket.Emit("send-position-event",
                JsonConvert.SerializeObject(target),
                JsonConvert.SerializeObject(args));
        }

        public static void PositionChangedRecieved(Action<DTO> onDataReieved)
        {
            socket.On("send-position-event", param =>
            {
                var args = JsonConvert.DeserializeObject<DTO>(param.ToString());
                onDataReieved(args);
            });
        }
    }
}
