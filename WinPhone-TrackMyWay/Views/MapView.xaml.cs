﻿using System;
using TrackMyWay.Api;
using TrackMyWay.Api.Models;
using Windows.ApplicationModel;
using Windows.Devices.Geolocation;
using Windows.Phone.UI.Input;
using Windows.System.Display;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Navigation;

namespace TrackMyWay.Views
{
    public sealed partial class MapView : Page
    {
        private UIElement marker;
        private Geolocator geolocator;
        private readonly DisplayRequest keepRequest;
        private User trackingUser;

        public MapView()
        {
            this.InitializeComponent();

            this.InitializeMap();

            this.keepRequest = new DisplayRequest();
            keepRequest.RequestActive();

            HardwareButtons.BackPressed += HardwareButtonsBackPressed;

            Application.Current.Resuming += OnPageResuming;
            Application.Current.Suspending += OnSuspending;
        }

        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
        }

        private void OnPageResuming(object sender, object e)
        {
        }

        private async void HardwareButtonsBackPressed(object sender, BackPressedEventArgs e)
        {
            if (e.Handled) return;

            e.Handled = true;

            var message = "Are your sure you want to disconnect?";
            var dialog = new MessageDialog(message);
            dialog.Commands.Add(new UICommand { Id = "Yes", Label = "Yes" });
            dialog.Commands.Add(new UICommand { Id = "No", Label = "No" });
            var res = await dialog.ShowAsync();
            bool yes = res.Id as string == "Yes";
            if (yes)
            {
                this.keepRequest.RequestRelease();
                Server.EmitDisconnect(this.trackingUser);
                AppNavigation.Navigate<UserContactsList>();
                HardwareButtons.BackPressed -= HardwareButtonsBackPressed;
            }
        }

        private void InitializeSocketConnection()
        {
            Server.PositionChangedRecieved(args =>
            {
                var geopoint = new Windows.Devices.Geolocation.Geopoint(new BasicGeoposition
                {
                    Latitude = args.Geopoint.Position.Latitude,
                    Longitude = args.Geopoint.Position.Longitude,
                    Altitude = 0
                }, AltitudeReferenceSystem.Terrain);

                SetMapPosition(geopoint);
            });
        }

        private async void SetMapPosition(Windows.Devices.Geolocation.Geopoint point)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
            {
                this.map.Center = point;

                MapControl.SetLocation(marker, point);
            });
        }

        private void InitializeMap()
        {
            this.marker = Markers.GetElipse();

            this.map.LandmarksVisible = true;
            this.map.ZoomLevel = 15;
            this.map.Children.Add(marker);
        }

        private void InitializeGeolocator()
        {
            this.geolocator = new Geolocator
            {
                DesiredAccuracy = PositionAccuracy.High,
                ReportInterval = 1000,
                DesiredAccuracyInMeters = 50,
            };
            geolocator.PositionChanged += GeolocatorPositionChanged;
            geolocator.StatusChanged += GeolocatorStatusChanged;
        }

        private void GeolocatorPositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            var dto = new DTO
            {
                Geopoint = new Api.Models.Geopoint
                {
                    Position = new Position
                    {
                        Latitude = args.Position.Coordinate.Point.Position.Latitude,
                        Longitude = args.Position.Coordinate.Point.Position.Longitude
                    }
                }
            };
            Server.EmitPosition(this.trackingUser, dto);
        }

        private void GeolocatorStatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var result = e.Parameter as RequestResult;

            if (result == null)
                return;

            this.trackingUser = result.SenderUser;
            this.InitializeSocketConnection();
            this.InitializeGeolocator();
        }

        //private async Task<Position> GetTelephonePosition()
        //{
        //    var geo = new Geolocator();
        //    var pos = await geo.GetGeopositionAsync();
        //    return new Position { Geopoint = pos.Coordinate.Point };
        //}
    }
}
