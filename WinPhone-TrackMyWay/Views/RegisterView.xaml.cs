﻿using System.Threading.Tasks;
using TrackMe.Api;
using TrackMyWay.Api;
using TrackMyWay.Api.Models;
using Windows.ApplicationModel.Contacts;
using Windows.System.UserProfile;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace TrackMyWay.Views
{
    public sealed partial class RegisterView : Page
    {
        public RegisterView()
        {
            this.InitializeComponent();

            this.BindCountrisComboBox();
        }

        private void BindCountrisComboBox()
        {
            Task.Factory.StartNew(() => CountryInfoList.Get)
            .ContinueWith(t =>
            {
                this.countriesCodeCombo.ItemsSource = t.Result;
                this.countriesCodeCombo.SelectedIndex = t.Result.FindIndex(c => c.Alpha2 == GlobalizationPreferences.HomeGeographicRegion);
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void OnRegister(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPhoneNumber.Text) && !string.IsNullOrEmpty(txtUserName.Text))
            {
                var countryInfo = ((CountryInfo)countriesCodeCombo.SelectedItem);

                string phone = string.Format("{0}{1}", countryInfo.CountryCallingCodes[0], txtPhoneNumber.Text);

                var user = new User
                {
                    FirstName = txtUserName.Text,
                    Phone = new ContactPhone { Number = phone, Kind = ContactPhoneKind.Mobile },
                    CountryCode = countryInfo.Alpha2
                };

                ClientDeviceSettings.SetCurrentUser(user);

                Server.RegisterUserInfo(user);

                AppNavigation.Navigate<UserContactsList>();
            }
        }
    }
}
