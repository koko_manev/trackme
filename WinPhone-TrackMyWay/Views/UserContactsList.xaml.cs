﻿using System;
using TrackMyWay.Api;
using TrackMyWay.Api.Models;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace TrackMyWay.Views
{
    public sealed partial class UserContactsList : Page
    {
        public UserContactsList()
        {
            this.InitializeComponent();

            this.LoadContacts();
        }

        private void LoadContacts()
        {
            Server.RegisteredForUsers(async users =>
            {
                var availableUsers = await ClientDeviceSettings.GetContacts(users);

                //ClientDeviceSettings.StoreAvailableUserInCache(availableUsers);

                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    list.ItemsSource = availableUsers;

                    this.txtMessage.Visibility = Visibility.Collapsed;
                    this.progressUserLoading.Visibility = Visibility.Collapsed;
                    this.backdrop.Visibility = Visibility.Collapsed;
                });
            });
            Server.EmitLoginUser();
            Server.OnRequestTracking(async sender =>
            {
                await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    var message = sender.FirstName + " wants to track your location";
                    var dialog = new MessageDialog(message, "Tracking request recieved");
                    dialog.Commands.Add(new UICommand { Id = "Allow", Label = "Allow" });
                    dialog.Commands.Add(new UICommand { Label = "Cancel" });
                    var res = await dialog.ShowAsync();
                    bool allow = res.Id as string == "Allow";
                    if (allow)
                        AppNavigation.Navigate<MapView>(new RequestResult { SenderUser = sender });
                    Server.RequestTrackingResponse(sender, allow);
                });
            });
        }

        private void OnStartContactTracking(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var user = btn.Tag as User;
            btn.Content = "sending request";
            if (user != null)
            {
                this.progressUserLoading.Visibility = Visibility.Visible;
                this.txtMessage.Visibility = Visibility.Visible;
                this.backdrop.Visibility = Visibility.Visible;
                this.txtMessage.Text = "Waiting for response...";

                Server.OnRequestTrackingResult(async result =>
                {
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                     {
                         if (result.IsAccepted)
                             AppNavigation.Navigate<MapView>(result);

                         this.progressUserLoading.Visibility = Visibility.Collapsed;
                         this.txtMessage.Visibility = Visibility.Collapsed;
                         this.backdrop.Visibility = Visibility.Collapsed;
                         btn.Content = "start tracking";
                     });

                    Server.OffRequestTrackingResult();
                });
                Server.RequestTracking(user);
            }
        }
    }
}
